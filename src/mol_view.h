/********************************************************************+
# Copyright 2018-2019 Daniel 'grindhold' Brendle
#
# This file is part of phex.
#
# phex is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later
# version.
#
# phex is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with phex.
# If not, see http://www.gnu.org/licenses/.
*********************************************************************/

#ifndef PHEX__MOL_VIEW_H_INCLUDED
#define PHEX__MOL_VIEW_H_INCLUDED

#include <phexfile.h>
#include <Elementary.h>
#include <Evas_GL.h>
#include <math.h>

#include "common.h"
#include "context.h"

void _generate_geometry(GLData* gld);

void _init_mol_gl(Evas_Object *obj);

void _draw_mol_gl(Evas_Object *obj);

#endif
