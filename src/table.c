/********************************************************************+
# Copyright 2018-2019 Daniel 'grindhold' Brendle
#
# This file is part of phex.
#
# phex is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later
# version.
#
# phex is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with phex.
# If not, see http://www.gnu.org/licenses/.
*********************************************************************/

#include "table.h"

void update_table(Phexdata* ctx) {
    GList* excitations = phexfile_simulation_get_excitations(ctx->sim);
    int n_exc = g_list_length(excitations);

    size_t coord_size = ctx->coord_len*sizeof(gint64);
    gint64* coord_param = (gint64*)malloc(coord_size);
    for (int i = 0; i < ctx->coord_len; i++) {
        *(coord_param+i) = (double)elm_slider_value_get(ctx->coord_sliders[i]);
    }

    GList* methods = phexfile_simulation_get_methods(ctx->sim);
    gchar* method = g_list_nth_data(methods, elm_radio_value_get(ctx->radio_method));
    char* target = (char*)malloc(sizeof(char)*512);

    for (int i = 0; i < n_exc; i++) {
         for (int j = 0; j < n_exc; j++) {
             if (i == j) continue;

             Evas_Object* label = ctx->energy_labels[i*(n_exc+1)+j];

             gchar* exc_i = g_list_nth_data(excitations, i);
             gchar* exc_j = g_list_nth_data(excitations, j);

             GError* err = NULL;

             double energy_i = phexfile_simulation_get_energy_fixed(
                                       ctx->sim, coord_param, ctx->coord_len, method, exc_i, &err
             );
             if (err != NULL) {
                g_error(err->message);
             }
             double energy_j = phexfile_simulation_get_energy_fixed(
                                        ctx->sim, coord_param, ctx->coord_len, method, exc_j, &err
             );
             if (err != NULL) {
                g_error(err->message);
             }

             double e_diff = fabs(energy_i-energy_j);
             double ev = e_diff * FAC_EV_HARTREE;
             double wl = H_EV_muM / ev;
             double freq = C / wl;

             sprintf(target, "%f a.u.<br>%f eV<br>%f THz<br>%f nm",
                              e_diff,    ev,      freq / 10e5, wl * 10e2);

             elm_object_text_set(label, target);
         }
    }
    free(target);
}
