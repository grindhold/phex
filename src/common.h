/********************************************************************+
# Copyright 2018-2019 Daniel 'grindhold' Brendle
#
# This file is part of phex.
#
# phex is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later
# version.
#
# phex is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with phex.
# If not, see http://www.gnu.org/licenses/.
*********************************************************************/

#ifndef PHEX__COMMON_H_INCLUDED
#define PHEX__COMMON_H_INCLUDED

#include <Evas_GL.h>

#include "context.h"

typedef struct _GLData GLData;

struct _GLData
{
   Phexdata*    ctx;
   Evas_GL_API  *glapi;
   //int        footest;
   GLuint       program;
   GLuint       vtx_shader;
   GLuint       fgmt_shader;
   GLuint       vbo;
   GLuint       vertexID;
   GLuint       colorID;
   GLuint       vertexID2;
   GLuint       colorID2;
   GLuint       mvpLoc;
   GLuint       positionLoc;
   GLuint       colorLoc;
   GLfloat      model[16], mvp[16];
   GLfloat      view[16];
   GLfloat      xangle;
   GLfloat      yangle;
   GLfloat      zangle;
   Eina_Bool    mouse_down : 1;
   Eina_Bool    initialized : 1;
   Evas_Object  *slx;
   Evas_Object  *sly;
   Evas_Object  *slz;
   float        slx_value;
   float        sly_value;
   float        slz_value;
   float*       vertices;
   float*       colors;
   unsigned int vertices_size;
   unsigned int colors_size;
};
//struct _GLData;

extern const float unit_matrix[16];

extern const float vertices[18];

extern const float colors[24];

extern const float palette[16];

void get_coords(Phexdata* pd, gint64* ret);

void customLoadIdentity(float matrix[16]);

void customMutlMatrix(float matrix[16], const float matrix0[16], const float matrix1[16]);
void customScale(float matrix[16], const float sx, const float sy, const float sz);

void customRotate(float matrix[16], const float anglex, const float angley, const float anglez);

int customFrustum(float result[16], const float left, const float right, const float bottom, const float top, const float near, const float far);

int init_shaders(GLData *gld);

char* color_f2h(float r, float g, float b);

char* colored_text(char* text, char* color);

#endif
