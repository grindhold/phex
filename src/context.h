/********************************************************************+
# Copyright 2018-2019 Daniel 'grindhold' Brendle
#
# This file is part of phex.
#
# phex is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later
# version.
#
# phex is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with phex.
# If not, see http://www.gnu.org/licenses/.
*********************************************************************/

#ifndef PHEX__CONTEXT_H_INCLUDED
#define PHEX__CONTEXT_H_INCLUDED

#include <phexfile.h>

typedef struct _Phexdata Phexdata;

struct _Phexdata {
    int coord_len;
    int num_excitations;
    PhexfileSimulation *sim;
    Evas_Object** coord_sliders;
    Evas_Object** energy_labels;
    Evas_Object* radio_x;
    Evas_Object* radio_y;
    Evas_Object* radio_method;
    int* coord_values;
    int* coord_axes;
    double graph_zoom;
};

#endif
