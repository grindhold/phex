/********************************************************************+
# Copyright 2018-2019 Daniel 'grindhold' Brendle
#
# This file is part of phex.
#
# phex is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later
# version.
#
# phex is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with phex.
# If not, see http://www.gnu.org/licenses/.
*********************************************************************/

#ifndef PHEX__TABLE_H_INCLUDED
#define PHEX__TABLE_H_INCLUDED

#include <phexfile.h>
#include <Elementary.h>

#include "context.h"

const double FAC_EV_HARTREE = 27.2113860217;
const double C = 299792458.0;
const double H_EV_muM = 1.23984193;

void update_table(Phexdata* ctx); 

#endif
