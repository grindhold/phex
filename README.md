Phex - the Potential Hypersurface EXplorer
==========================================

![Screenshot of Phex](https://www.notabug.org/grindhold/phex/raw/master/screenshots/phex.png)

Phex is an application that visualizes and navigates potential energy surfaces (PES) of
molecules along several coordinates.

Dependencies
------------

  * elementary
  * phexfile-0.2

Building
--------

TBD

License
-------

Phex is published under the GNU General Public License v3 or later.
